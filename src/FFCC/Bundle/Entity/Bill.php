<?php
namespace FFCC\Bundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 */
class Bill
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $inputMethod;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $inputSource;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $details;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $due;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $paid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="FFCC\Bundle\Entity\Account", inversedBy="bill")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     * @ORM\JoinColumn(name="account_unique_id", referencedColumnName="uniqueId")
     */
    private $account;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inputMethod
     *
     * @param integer $inputMethod
     * @return Bill
     */
    public function setInputMethod($inputMethod)
    {
        $this->inputMethod = $inputMethod;

        return $this;
    }

    /**
     * Get inputMethod
     *
     * @return integer 
     */
    public function getInputMethod()
    {
        return $this->inputMethod;
    }

    /**
     * Set inputSource
     *
     * @param string $inputSource
     * @return Bill
     */
    public function setInputSource($inputSource)
    {
        $this->inputSource = $inputSource;

        return $this;
    }

    /**
     * Get inputSource
     *
     * @return string 
     */
    public function getInputSource()
    {
        return $this->inputSource;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return Bill
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Bill
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Bill
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set due
     *
     * @param \DateTime $due
     * @return Bill
     */
    public function setDue($due)
    {
        $this->due = $due;

        return $this;
    }

    /**
     * Get due
     *
     * @return \DateTime 
     */
    public function getDue()
    {
        return $this->due;
    }

    /**
     * Set paid
     *
     * @param \DateTime $paid
     * @return Bill
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return \DateTime 
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Bill
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set account
     *
     * @param \FFCC\Bundle\Entity\Account $account
     * @return Bill
     */
    public function setAccount(\FFCC\Bundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \FFCC\Bundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
