<?php

namespace FFCC\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    public function ClientOnLoginSuccessAction()
    {
        return new Response("success");
    }
}
