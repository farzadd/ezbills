<?php

namespace FFCC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * 
 * @ORM\Entity(repositoryClass="FFCC\Bundle\Entity\ServiceRepository")
 */
class Service
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="FFCC\Bundle\Entity\Provider", inversedBy="service")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="id")
     */
    private $provider;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=true, name="name")
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="FFCC\Bundle\Entity\Account", mappedBy="service")
     */
    private $account;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set provider
     *
     * @param \stdClass $provider
     * @return Service
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \stdClass 
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set account
     *
     * @param \FFCC\Bundle\Entity\Account $account
     * @return Service
     */
    public function setAccount(\FFCC\Bundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \FFCC\Bundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
