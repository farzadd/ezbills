<?php
namespace FFCC\Bundle\Auth;
 
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUserProvider;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use FFCC\Bundle\Entity\User;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
 
class OAuthProvider extends OAuthUserProvider
{
    protected $session, $doctrine, $admins;
 
    public function __construct($session, $doctrine, $service_container)
    {
        $this->session = $session;
        $this->doctrine = $doctrine;
        $this->container = $service_container;
    }
 
    public function loadUserByUsername($username)
    {
        $qb = $this->doctrine->getManager()->createQueryBuilder();
        $qb->select('u')
            ->from('EZBillsBundle:User', 'u')
            ->where('u.googleId = :gid')
            ->setParameter('gid', $username)
            ->setMaxResults(1);
        $result = $qb->getQuery()->getResult();
 
        if (count($result)) {
            return $result[0];
        } else {
            return new User();
        }
    }
 
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        //Data from Google response
        $google_id = $response->getUsername(); /* An ID like: 112259658235204980084 */
        $email = $response->getEmail();
        //$nickname = $response->getNickname();
        $realname = $response->getRealName();
        $avatar = $response->getProfilePicture();
        
        if (!$google_id)
            return null;
 
        //Check if this Google user already exists in our app DB
        $qb = $this->doctrine->getManager()->createQueryBuilder();
        $qb->select('u')
            ->from('EZBillsBundle:User', 'u')
            ->where('u.googleId = :gid')
            ->setParameter('gid', $google_id)
            ->setMaxResults(1);
        $result = $qb->getQuery()->getResult();
 
        //add to database if doesn't exists
        if (!count($result)) {
            $user = new User();
            $user->setRealname($realname);
            $user->setEmail($email);
            $user->setGoogleId($google_id);
            $user->setAvatar($avatar);


            //create two new labels here
            $token = $response->getAccessToken();
            //$this->session->get('security.context')->getToken()->getAccessToken();

            $token_url = "?access_token=" . $token ;
        
            $url = "https://www.googleapis.com/gmail/v1/users/me"
                . "/labels/" . $token_url;

            $data = array("labelListVisibility" => "labelShow", 
                          "messageListVisibility" => "hide",
                          "name" => "EZBills" );
            $data_string = json_encode( $data );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_URL, $url );
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                                array('Content-type: application/json',
                                'Content-Length: ' . strlen($data_string)));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);
            
            error_log( $response );
            
            $this->session->getFlashBag()->add('notice', "1. " . $response );
            
            $data = array("labelListVisibility" => "labelHide", 
                          "messageListVisibility" => "hide",
                          "name" => "EZBilled" );
            $data_string = json_encode( $data );

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_URL, $url );
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                                array('Content-type: application/json',
                                'Content-Length: ' . strlen($data_string)));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);
            
            $this->session->getFlashBag()->add('notice', "2. " . $response );
            //////////

            //Set some wild random pass since its irrelevant, this is Google login
            $factory = $this->container->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword(md5(uniqid()), $user->getSalt());
            $user->setPassword($password);
 
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
        } else {
            $user = $result[0]; /* return User */
            $user->setAvatar($avatar);
            
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
        }
 
        return $this->loadUserByUsername($response->getUsername());
    }
 
    public function supportsClass($class)
    {
        return $class === 'FFCC\\Bundle\\Entity\\User';
    }
}
