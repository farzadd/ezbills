<?php
 
namespace FFCC\Bundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
 
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUser;
 
/**
 * User
 *
 * @ORM\Table(name="User")
 * @ORM\Entity(repositoryClass="FFCC\Bundle\Entity\UserRepository")
 * @UniqueEntity("email")
 */
class User extends OAuthUser implements EquatableInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
 
    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true, length=255, nullable=true, name="google_id")
     */
    protected $googleId;
 
    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=false, length=32, nullable=true, name="role")
     */
    protected $role;
 
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=true, name="realname")
     */
    protected $realname;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=true, name="avatar")
     */
    protected $avatar;
 
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=true, name="salt")
     */
    protected $salt;
 
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="password")
     */
    protected $password;
 
    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true, length=255, nullable=true, name="email")
     */
    protected $email;
 
    /**
     * @ORM\Column(type="boolean", nullable=true, name="is_active")
     */
    protected $isActive;

    /**
     * @ORM\OneToMany(targetEntity="FFCC\Bundle\Entity\Account", mappedBy="user")
     */
    private $account;
 
    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }
 
    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
 
    public function __construct()
    {
        $this->roles = "ROLE_USER";
 
        $this->isActive = true;
        $this->salt = md5(uniqid(null, true));
    }
 
    /**
     * @param string $realname
     */
    public function setRealname($realname)
    {
        $this->realname = $realname;
    }
 
    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
    
    /**
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
 
    /**
     * @return string
     */
    public function getRealname()
    {
        return $this->realname;
    }
 
    /**
     * @param string $googleId
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
    }
 
    /**
     * @return string
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }
    
    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->email;
    }
 
    public function getRole()
    {
        return $this->role;
    }
    
    public function setRole($role)
    {
        $this->role = $role;
    }
 
 
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
 
    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
 
        return $this;
    }
 
    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }
 
    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
 
        return $this;
    }
 
    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
 
    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
 
        return $this;
    }
 
    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
 
    /**
     * @inheritDoc
     */
 
    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }
 
    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }
 
    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }
 
    public function isEqualTo(UserInterface $user)
    {
        if ((int)$this->getId() === $user->getId()) {
            return true;
        }
 
        return false;
    }

    /**
     * Add account
     *
     * @param \FFCC\Bundle\Entity\Account $account
     * @return User
     */
    public function addAccount(\FFCC\Bundle\Entity\Account $account)
    {
        $this->account[] = $account;

        return $this;
    }

    /**
     * Remove account
     *
     * @param \FFCC\Bundle\Entity\Account $account
     */
    public function removeAccount(\FFCC\Bundle\Entity\Account $account)
    {
        $this->account->removeElement($account);
    }

    /**
     * Get account
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccount()
    {
        return $this->account;
    }
}
