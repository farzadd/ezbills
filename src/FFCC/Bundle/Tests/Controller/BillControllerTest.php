<?php

namespace FFCC\Bundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BillControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/bills');
    }

    public function testView()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/bills/view/{id}');
    }

    public function testPay()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/bills/pay/{id}');
    }

}
