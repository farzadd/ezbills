<?php

namespace FFCC\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FFCC\Bundle\Entity\User;

class BillType extends AbstractType
{
    protected $user;

    public function __construct (User $user)
    {
        $this->user = $user;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->user;
        
        $builder
            ->add('account', 'entity', array(
                'class' => 'EZBillsBundle:Account',
                'query_builder' => function($er) use ($user) {
                    return $er->createQueryBuilder('a')
                        ->where('a.user = :user')
                        ->setParameter('user', $user);
                }))
            ->add('amount', 'money', array(
                'currency' => 'USD'))
            ->add('details')
            ->add('due', 'date', array(
                'widget' => 'single_text'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FFCC\Bundle\Entity\Bill'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ffcc_bundle_bill';
    }
}
