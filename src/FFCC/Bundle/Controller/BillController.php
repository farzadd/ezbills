<?php

namespace FFCC\Bundle\Controller;

use FFCC\Bundle\Entity\Bill;
use FFCC\Bundle\Form\BillType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BillController extends Controller
{
    public function indexAction()
    {
        $user = $this->getUser();
        $bill = new Bill();
        $form = $this->createForm(new BillType($user), $bill, array('action' => $this->generateUrl('_bills_manual_add')));
        
        $accounts = $this->getDoctrine()
            ->getRepository('EZBillsBundle:Account')
            ->findAll();
            
        $allBills = array();
        foreach ($accounts as $account)
        {
            $bills = $this->getDoctrine()
                ->getRepository('EZBillsBundle:Bill')
                ->findBy(
                    array('account' => $account));
                
            foreach ($bills as $bill)
            {
                if (!in_array($bill, $allBills))
                {
                    array_push($allBills, $bill);
                }
            }
        }
        
        if (count($allBills) > 1)
            usort($allBills, function ($a, $b) { return  $a->getDue() > $b->getDue(); });
        
        return $this->render('EZBillsBundle:Bill:index.html.twig', array(
                'bills' => $allBills,
                'form' => $form->createView()
            ));
    }

    public function viewAction($id)
    {
        return $this->render('EZBillsBundle:Bill:view.html.twig', array(
                // ...
            ));
    }

    public function manualAddAction()
    {
        $user = $this->getUser();
        $bill = new Bill();
        $form = $this->createForm(new BillType($user), $bill, array('action' => $this->generateUrl('_bills_manual_add')));

        $request = $this->container->get('request');
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $bill->setInputMethod(0);
            $bill->setStatus(1);
            $bill->setCreated(new \DateTime("now"));
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($bill);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('notice','Bill #' . $bill->getId() . ' as been added!');
        }
        else
            $this->get('session')->getFlashBag()->add('error','Unable to add bill!');
        
        return $this->redirect($this->generateUrl("_bills"));
    }
    
    public function payAction($id)
    {
        $user = $this->getUser();
        
        $bill = $this->getDoctrine()
            ->getRepository('EZBillsBundle:Bill')
            ->find($id);
         
        if (!$bill)
        {
            $this->get('session')->getFlashBag()->add('error','We could not find that bill.');
            return $this->indexAction();
        } else if ($user != $bill->getAccount()->getUser())
        {
            $this->get('session')->getFlashBag()->add('error','That bill does not belong to you.');
            return $this->indexAction();
        } else if ($bill->getStatus() == 2)
        {
            $this->get('session')->getFlashBag()->add('error','That bill is already marked as paid.');
            return $this->indexAction();
        }
        
        $bill->setStatus(2);
        $em = $this->getDoctrine()->getManager();
        $em->persist($bill);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add('notice','Bill #' . $bill->getId() . ' marked as paid!');
        
        return $this->redirect($this->generateUrl("_bills"));
    }

    public function deleteAction($id)
    {
        $user = $this->getUser();
        
        $bill = $this->getDoctrine()
            ->getRepository('EZBillsBundle:Bill')
            ->find($id);
        
        if (!$bill)
        {
            $this->get('session')->getFlashBag()->add('error','We could not find that bill.');
            return $this->indexAction();
        }        
        else if ($user != $bill->getAccount()->getUser())
        {
            $this->get('session')->getFlashBag()->add('error','That bill does not belong to you.');
            return $this->indexAction();
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($bill);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add('notice','Bill #' . $bill->getId() . ' deleted!');
        
        return $this->redirect($this->generateUrl("_bills"));
    }
}
