<?php

namespace FFCC\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Provider
 *
 * 
 * @ORM\Entity(repositoryClass="FFCC\Bundle\Entity\ProviderRepository")
 */
class Provider
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=true, name="name")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="webUrl")
     */
    private $webUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="loginUrl")
     */
    private $loginUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, nullable=true, name="avatarName")
     */
    private $avatarName;

    /**
     * @ORM\OneToMany(targetEntity="FFCC\Bundle\Entity\Service", mappedBy="provider")
     */
    private $service;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Provider
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set webUrl
     *
     * @param string $webUrl
     * @return Provider
     */
    public function setWebUrl($webUrl)
    {
        $this->webUrl = $webUrl;

        return $this;
    }

    /**
     * Get webUrl
     *
     * @return string 
     */
    public function getWebUrl()
    {
        return $this->webUrl;
    }

    /**
     * Set loginUrl
     *
     * @param string $loginUrl
     * @return Provider
     */
    public function setLoginUrl($loginUrl)
    {
        $this->loginUrl = $loginUrl;

        return $this;
    }

    /**
     * Get loginUrl
     *
     * @return string 
     */
    public function getLoginUrl()
    {
        return $this->loginUrl;
    }

    /**
     * Set avatarName
     *
     * @param string $avatarName
     * @return Provider
     */
    public function setAvatarName($avatarName)
    {
        $this->avatarName = $avatarName;

        return $this;
    }

    /**
     * Get avatarName
     *
     * @return string 
     */
    public function getAvatarName()
    {
        return $this->avatarName;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->service = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add service
     *
     * @param \FFCC\Bundle\Entity\Service $service
     * @return Provider
     */
    public function addService(\FFCC\Bundle\Entity\Service $service)
    {
        $this->service[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \FFCC\Bundle\Entity\Service $service
     */
    public function removeService(\FFCC\Bundle\Entity\Service $service)
    {
        $this->service->removeElement($service);
    }

    /**
     * Get service
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getService()
    {
        return $this->service;
    }
}
