<?php

namespace FFCC\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FFCC\Bundle\Controller\API;
use FFCC\Bundle\Entity\Bill;

class GmailParserController extends Controller
{
    public function readAndParseAction()
    {
        $user = $this->getUser();
        
        $token = $this->get('security.context')->getToken()->getAccessToken();
        $token_url = "?access_token=" . $token ;
        
        $url = "https://www.googleapis.com/gmail/v1/users/me/labels/" . $token_url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        $data = json_decode($response,true);
        
        $labels = $data[ 'labels' ];
        
        $ezbills_label = '';
        $ezbilled_label = '';

        foreach ( $labels as $label )
        {
            if ( $label [ 'name' ] == 'EZBills' )
                $ezbills_label = $label[ 'id' ];
            if ( $label [ 'name' ] == 'EZBilled' )
                $ezbilled_label = $label[ 'id' ];        
        }        
        $labeldata = array(
                        "addLabelIds" => array( $ezbilled_label ), 
                        "removeLabelIds" => array( $ezbills_label ));
                      
        $labeldatastring = json_encode( $labeldata );
        
        $url = "https://www.googleapis.com/gmail/v1/users/me"
                . "/messages/" . $token_url . "&q=\"label:EZBills\"";

        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        $data = json_decode($response,true);
        
        $success = 0;
        $fail = 0;
        
        if (!array_key_exists("messages", $data))
        {
           $this->get('session')->getFlashBag()->add('error', 
                        "You have no emails with the 'EZBills' Tag" );
                        
            return $this->redirect($this->generateUrl("_index")); 
        }
        
        foreach( $data["messages"] as $message )
        {
            $messageid = $message[ "id" ];

            $url = "https://www.googleapis.com/gmail/v1/users/me"
                . "/messages/" . $messageid . "/"
                . $token_url . '&format=raw';

            curl_setopt($ch, CURLOPT_URL, $url );
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);

            $mail = json_decode($response,true);

            // Decode the email
            $raw = $mail["raw"];
            $raw = str_replace ( '-' , '+' , $raw );
            $raw = str_replace ( '_' , '/' , $raw );
            $raw = base64_decode($raw);
            
            // Retrieve the email the message was sent from
            $matches = array();
            preg_match("/(?:From\:\s[^@]+@)([^\s]*)/", $raw, $matches);
            $from = $matches[1];
            
            // Trim any subdomain
            $from = preg_replace ('/^[^.]*\.(?=\w+\.\w+$)/', '' , $from );
            
            // Grab the provider based on domain name
            $provider = $this->getDoctrine()
                ->getRepository('EZBillsBundle:Provider')
                ->findOneBy(array('webUrl' => $from));
            
            // Grab amount, date and uniqueId if possible
            $billData = array (
                "amount" => null,
                "date" => null,
                "uniqueId" => null,
            );
            
            // Double array that holds the Regex Expressions
            $expr = array(
                "fido.ca" => array (
                    "amount" => '/(?:total: \<strong\>\$)([^<]*)/',
                    "date" => '/(?:Due date\: \<strong\>)([^<]*)/',
                    "uniqueId" => '/(?:account number \<strong\>)([^<]*)/',
                ),
            );
            
            // Check to see if we support the provider
            if (!array_key_exists($from, $expr))
            {
                // Fail and continue
                $fail++;
                continue;
            }
            
            // Match the Regex and grab the billData
            foreach ($expr[$from] as $key => $value)
            {
                preg_match($value, $raw, $matches);
                if (count($matches) > 1)
                    $billData[$key] = $matches[1];
            }
            
            // Call the API to store the new bill object
            $bill = new Bill();
            
            $account = $this->getDoctrine()
                ->getRepository('EZBillsBundle:Account')
                ->find(1);
            
            $bill->setAccount($account);
            $bill->setInputMethod(1);
            $bill->setStatus(0);
            $bill->setCreated(new \DateTime("now"));
            
            if ($billData["amount"])
                $bill->setAmount($billData["amount"]);
            
            if ($billData["date"])
                $bill->setDue(new \DateTime($billData["date"]));
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($bill);
            $em->flush();
            
            // Deactivate the label
            $url = "https://www.googleapis.com/gmail/v1/users/me"
               . "/messages/" . $messageid . "/modify/"
               .  $token_url;
            
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $labeldatastring);
            curl_setopt($ch, CURLOPT_URL, $url );
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                                array('Content-type: application/json',
                                'Content-Length: ' . strlen($labeldatastring)));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            
            // Count as a success!
            $success++;
        }
        
        if ($success == 0 && $fail == 0)
            $this->get('session')->getFlashBag()->add('notice', "No attempt was made at parsing." );
        else
        {
            if ($success > 0)
                $this->get('session')->getFlashBag()->add('notice', $success . " emails successfully converted." );

            if ($fail > 0)
                $this->get('session')->getFlashBag()->add('error', $fail . " emails failed to convert." );
        }
        
        return $this->redirect($this->generateUrl("_index"));
    }
}
