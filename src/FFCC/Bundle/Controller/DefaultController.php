<?php

namespace FFCC\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends BaseController
{
    public function indexAction()
    {
        return $this->render('EZBillsBundle:Default:index.html.twig');
    }
}
