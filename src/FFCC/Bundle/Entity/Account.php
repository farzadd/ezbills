<?php
namespace FFCC\Bundle\Entity;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 */
class Account
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    private $uniqueId;

    /**
     * @ORM\OneToOne(targetEntity="FFCC\Bundle\Entity\Service", inversedBy="account")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", unique=false)
     */
    private $service;

    /**
     * @ORM\OneToMany(targetEntity="FFCC\Bundle\Entity\Bill", mappedBy="account")
     */
    private $bill;

    /**
     * @ORM\ManyToOne(targetEntity="FFCC\Bundle\Entity\User", inversedBy="account")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bill = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uniqueId
     *
     * @param string $uniqueId
     * @return Account
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;

        return $this;
    }

    /**
     * Get uniqueId
     *
     * @return string 
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Set service
     *
     * @param \FFCC\Bundle\Entity\Service $service
     * @return Account
     */
    public function setService(\FFCC\Bundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \FFCC\Bundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Add bill
     *
     * @param \FFCC\Bundle\Entity\Bill $bill
     * @return Account
     */
    public function addBill(\FFCC\Bundle\Entity\Bill $bill)
    {
        $this->bill[] = $bill;

        return $this;
    }

    /**
     * Remove bill
     *
     * @param \FFCC\Bundle\Entity\Bill $bill
     */
    public function removeBill(\FFCC\Bundle\Entity\Bill $bill)
    {
        $this->bill->removeElement($bill);
    }

    /**
     * Get bill
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * Set user
     *
     * @param \FFCC\Bundle\Entity\User $user
     * @return Account
     */
    public function setUser(\FFCC\Bundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \FFCC\Bundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * String representation of this object
     * @return string
     */
    public function __toString()
    {
        try
        {
            return (string) ($this->service->getProvider()->getName() . ": " . $this->uniqueId);
        } catch (Exception $ex)
        {
        }
        
        return "";
    }
}
