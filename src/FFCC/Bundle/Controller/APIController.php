<?php

namespace FFCC\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class APIController extends Controller
{
    public function createBillAction($providerId, $amount, $date, $uniqueId)
    {
        $bill = new Bill();
        
        $provider = $this->getDoctrine()
            ->getRepository('EZBillsBundle:Provider')
            ->find($providerId);
            
        $accounts = array();
        foreach ($provider->getServices() as $service)
            $accounts[] = $service->getAccounts();
        
        $ourAccount = null;
        foreach ($accounts as $account)
        {
            if ($account->getUniqueId() == $uniqueId)
            {
                $ourAccount = $account;
                break;
            }
        }
        
        if ($ourAccount == null)
            return new Response('fail');
        
        $bill->setAccount($ourAccount);
        $bill->setInputMethod(1);
        $bill->setStatus(0);
        $bill->setCreated(new \DateTime("now"));
        
        if ($amount)
            $bill->setAmount($amount);
        
        if ($date)
            $bill->setDue($date);
        
        if ($amount)
            $bill->setAmount($amount);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($bill);
        $em->flush();
        
        return new Response('success');
    }

}
