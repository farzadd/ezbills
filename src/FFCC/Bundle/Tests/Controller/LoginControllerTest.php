<?php

namespace FFCC\Bundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginControllerTest extends WebTestCase
{
    public function testClientonloginsuccess()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/ClientOnLoginSuccess');
    }

    public function testServerlogincallback()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/ServerLoginCallback');
    }

}
